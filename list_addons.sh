#!/usr/bin/env bash

### ----------------------------------------------------------------------
### doc
### ----------------------------------------------------------------------

### List add-ons in all local Firefox profiles. Requires:

### * users to know where they keep their Firefox profiles.
### * python, to parse {profiles INI, add-ons JSON}

### CAUTION:

### 1. When parsing `profiles.ini`, this code assumes that file has only sections with names like 'General', 'Profile0', 'Profile1', ...
###   I believe this assumption is valid, but YMMV. (Please lemme know if you know it's false.)

### 1. When parsing a profile, this code assumes that
###    * its add-ons are listed in a file with name @ FIREFOX_ADDONS_FN (see definition below)
###    * the add-ons list file is in the root of the profile dir/folder

### 2. This has been tested only
###    * on Linux. Please lemme know about your experience on other platforms.
###    * with Python versions={2.7.9, 3.4.2}. Please lemme know about your experience with other versions.

### Your pull requests are welcome! and will be credited if used.

### TODO:
### * rewrite in pure {Bash, Python}

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

## messaging

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))" # FQ/absolute path
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## OS properties: override if you dare.

MY_PYTHON='python'
# Valid alternatives on my Linux boxes (YMMV) include:
# MY_PYTHON='/usr/bin/python2'
# MY_PYTHON='/usr/bin/python3'

## Firefox properties: override if you dare.
## 'FN' == filename, 'FP' == FQ filepath

FIREFOX_CONFIG_DIR="${HOME}/.mozilla/firefox"  # default Firefox value
FIREFOX_PROFILE_INI_FN='profiles.ini'          # default Firefox value
FIREFOX_ADDONS_FN='addons.json'                # default Firefox value

## initialization

FIREFOX_PROFILE_INI_FP="${FIREFOX_CONFIG_DIR}/${FIREFOX_PROFILE_INI_FN}"
FIREFOX_PROFILE_DIR_LIST=''                    # default empty
#FIREFOX_ADDONS_FP_LIST=''                      # default empty

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

### check dependencies

if   [[ -z "${FIREFOX_CONFIG_DIR}" ]] ; then
    >&2 echo "${ERROR_PREFIX} FIREFOX_CONFIG_DIR not defined, exiting ..."
    exit 2
elif [[ ! -r "${FIREFOX_CONFIG_DIR}" ]] ; then
    >&2 echo "${ERROR_PREFIX} cannot read FIREFOX_CONFIG_DIR='${FIREFOX_CONFIG_DIR}', exiting ..."
    exit 3
elif [[ -z "${FIREFOX_PROFILE_INI_FP}" ]] ; then
    >&2 echo "${ERROR_PREFIX} FIREFOX_PROFILE_INI_FP not defined, exiting ..."
    exit 3
elif [[ ! -r "${FIREFOX_PROFILE_INI_FP}" ]] ; then
    >&2 echo "${ERROR_PREFIX} cannot read Firefox profiles file='${FIREFOX_PROFILE_INI_FP}', exiting ..."
    exit 4
elif [[ -z "$(which python)" ]] ; then
    >&2 echo "${ERROR_PREFIX} ${THIS_FN} requires 'python' not found on PATH, exiting ..."
    exit 5
fi

### Get list of profile dirs, build list of add-ons JSON files

## Parse profiles file using python, so
## * gotta export the envvars
export FIREFOX_PROFILE_INI_FP
export FIREFOX_ADDONS_FN
#export FIREFOX_ADDONS_FP_LIST
## * indenting becomes important

## I'd like to use parsed values to build FIREFOX_ADDONS_FP_LIST , but
## ya can't modify the parent process. So instead use Python ...
## but note the import for the ini-file config parser
## * import name=ConfigParser in python 2
## * import name=configparser in python 3
## * ... but *class* name=ConfigParser for both

# ${MY_PYTHON} --version # debugging
${MY_PYTHON} -c '
import sys
try:
    # for python version >= 3
    import configparser as Config_Parser
except ImportError:
#    print("Cannot import configparser, retrying for python version < 3:")
    try:
        import ConfigParser as Config_Parser
    except ImportError:
        sys.stderr.write("Cannot import configparser or ConfigParser: cannot handle your Python, exiting ...")
        sys.exit(6)

import os
profile_ini_fp = os.environ.get("FIREFOX_PROFILE_INI_FP")
profile_addons_fn = os.environ.get("FIREFOX_ADDONS_FN")

parser = Config_Parser.ConfigParser()
parser.read(profile_ini_fp)

## Parse the profile dirs/folders
# fails: profile_names = parser.sections().remove("General")
profile_names = parser.sections()
# fails: profile_names.remove('General')
profile_names.remove("General")
# print(profile_names) # debugging
for profile_name in profile_names:
    profile_path = parser.get(profile_name, "Path")
    profile_addons_fp = os.path.join(profile_path, profile_addons_fn)

    ## Parse the JSON file (if found) to get list of add-ons
    import json
    try:
        with open(profile_addons_fp) as addons_file:
            print(profile_name + " @ " + profile_path + " has add-ons @ " + profile_addons_fp + " :")
            addons_data = json.load(addons_file)
            for (i, addon) in enumerate(addons_data["addons"]):
                print("    add-on name=" + addon["name"])
                print("        version=" + addon["version"])
                print("            URI=" + addon["learnmoreURL"])
                print("") # newline
    except IOError as e:
        print(profile_name + " has no add-ons JSON @ " + profile_addons_fp)


    print("") # newline
'

exit 0
